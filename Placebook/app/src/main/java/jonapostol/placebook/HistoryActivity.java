package jonapostol.placebook;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;

import java.util.ArrayList;


public class HistoryActivity extends ActionBarActivity implements ActionMode . Callback {
    private ListView mListview;
    private ArrayList<PlacebookEntry> mPlacebookEntries;
    protected Object mActionMode;

    public int selectedItem = -1;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListview = (ListView) findViewById (R.id.listview);
        setContentView(R.layout.activity_history);
        Intent intent = getIntent();
        mPlacebookEntries = intent.getParcelableArrayListExtra(MainActivity.VIEW_ALL_KEY);
        CharSequence text = "What is " + mPlacebookEntries.get(0).getName();
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();


        if(!mPlacebookEntries.isEmpty()) {
            try {
                EntryAdapter ea = new EntryAdapter(this, mPlacebookEntries);

                mListview.setAdapter(ea);
            } catch (Exception e){
                text = "Error: " + e;
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
            }
//            mListview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//                @Override
//                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                    if (mActionMode != null)
//                        return false;
//                    selectedItem = position;
//                    mActionMode = HistoryActivity.this.startActionMode(HistoryActivity.this);
//                    view.setSelected(true);
//                    return true;
//                }
//            });
        }
    }

    @Override
    public boolean onCreateActionMode (ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.rowselection, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode , Menu menu){
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch ( item . getItemId () ) {
            case R.id.action_delete_place:
                // Delete Item
                mode.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode){
        mActionMode = null;
        selectedItem = -1;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy () ;
        Intent intent = new Intent () ;
        intent.putParcelableArrayListExtra(MainActivity.VIEW_ALL_KEY, mPlacebookEntries);
        setResult(Activity.RESULT_OK, intent);
    }


}
