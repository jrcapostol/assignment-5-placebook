package jonapostol.placebook;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Jon Apostol on 6/7/2015.
 */
public class EntryAdapter extends ArrayAdapter<PlacebookEntry> {

    private ArrayList<PlacebookEntry> mPlacebookEntries;
    private Activity context;

    public EntryAdapter(Activity context, ArrayList<PlacebookEntry> pbe){
        super(context, R.layout.row_layout, pbe);
        this.context = context;
        mPlacebookEntries = pbe;
        Log.d("TEST", "PlaceName: " + mPlacebookEntries.get(0).getName());
        Log.d("TEST", "Filename: " + mPlacebookEntries.get(0).getPhotoPath());
        Log.d("TEST", "PlaceDescription: " + mPlacebookEntries.get(0).getDescription());
    }

    public View getView(int pos, View convertView, ViewGroup parent){
        File file = new File(mPlacebookEntries.get(pos).getPhotoPath());

        Log.d("TEST", "PlaceName: " + mPlacebookEntries.get(pos).getName());
        Log.d("TEST", "Filename: " + mPlacebookEntries.get(pos).getPhotoPath());
        Log.d("TEST", "PlaceDescription: " + mPlacebookEntries.get(pos).getDescription());

        Bitmap display = BitmapFactory.decodeFile(file.getAbsolutePath());

        LayoutInflater inflater = context.getLayoutInflater();
        View row = inflater.inflate(R.layout.row_layout, null, true);
        TextView place, description;
        ImageView pic;
        place = (TextView) row.findViewById(R.id.row_txtPlace);
        description = (TextView) row.findViewById(R.id.row_txtPlaceDesc);
        pic=(ImageView)row.findViewById(R.id.row_image_view);
        place.setText(mPlacebookEntries.get(pos).getName());
        description.setText(mPlacebookEntries.get(pos).getDescription());
        pic.setImageBitmap(display);

        return row;
    }
}
