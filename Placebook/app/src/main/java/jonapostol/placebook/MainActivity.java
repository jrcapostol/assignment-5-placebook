package jonapostol.placebook;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends ActionBarActivity{

    public static final String VIEW_ALL_KEY = "jonapostol.placebook.EXTRA_VIEW_ALL";
    private static final int REQUEST_IMAGE_CAPTURE = 1001;
    private static final int REQUEST_VIEW_ALL = 1005;
    private static final int REQUEST_SPEECH_INPUT = 1002;
    private static final int REQUEST_PLACE_PICKER = 1003;

    private ArrayList< PlacebookEntry > mPlacebookEntries;
    private GoogleApiClient mGoogleApiClient;

    private int highlighted = R.id.edit_place_desc;

    //INFO TO STORE IN PLACEBOOKENTRY
    private PlacebookEntry pbe;
    private int id;
    private String placeName;
    private String placeDesc;
    private String picLocation;
    private File photoFile;

    //LOCATOR
    Location location;
    double longitude;
    double latitude;
    double altitude;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        mPlacebookEntries = new ArrayList<PlacebookEntry>();
        super.onCreate(savedInstanceState);
        initGoogleApi();
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch(id){
            case R.id.action_search:
                return true;
            case R.id.action_new_place:
                placeName = ((EditText) findViewById(R.id.txtPlaceContent)).getText().toString();
                placeDesc = ((EditText) findViewById(R.id.edit_place_desc)).getText().toString();
                pbe = new PlacebookEntry();
                pbe.setId(mPlacebookEntries.size());
                pbe.setName(placeName);
                pbe.setDescription(placeDesc);
                pbe.setPhotoPath(picLocation);
                mPlacebookEntries.add(pbe);
                CharSequence text0 = "Placebook Entry added. Place: " + placeName;
                Toast.makeText(getApplicationContext(), text0, Toast.LENGTH_SHORT).show();
                dispatchViewAllPlaces();
                try {
                    placeName = ((EditText) findViewById(R.id.txtPlaceContent)).getText().toString();
                    placeDesc = ((EditText) findViewById(R.id.edit_place_desc)).getText().toString();
                    pbe = new PlacebookEntry();
                    pbe.setId(mPlacebookEntries.size());
                    pbe.setName(placeName);
                    pbe.setDescription(placeDesc);
                    pbe.setPhotoPath(picLocation);
                    if(placeName == null || placeDesc == null || picLocation == null)
                        throw new Exception();
                    mPlacebookEntries.add(pbe);
                    CharSequence text1 = "Placebook Entry added. Place: " + placeName;
                    Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                    dispatchViewAllPlaces();
                } catch (Exception e){
                    String txt = "";
                    if(placeName == null){
                        txt += "placeName ";
                    }
                    if(placeDesc == null){
                        txt += "placeDesc ";
                    }
                    if(picLocation == null){
                        txt += "picLocation";
                    }
                    CharSequence text = "Info is missing: " + txt;
                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_view_all:
                dispatchViewAllPlaces();
                return true;
            case R.id.action_edit_place:
                return true;
            case R.id.action_delete_place:
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    // Call dispatchViewAllPlaces() when its menu command is selected.
    private void dispatchViewAllPlaces () {
        Intent intent = new Intent (this, HistoryActivity.class);
        intent.putParcelableArrayListExtra(VIEW_ALL_KEY, mPlacebookEntries);
        try{
            startActivityForResult ( intent , REQUEST_VIEW_ALL );
        } catch (ActivityNotFoundException a) {}
    }
    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {

        //FOR READING ALL
        if ( resultCode == RESULT_OK && requestCode == REQUEST_VIEW_ALL && data != null ) {
            ArrayList <PlacebookEntry> placebookEntrys =
                    data.getParcelableArrayListExtra(VIEW_ALL_KEY);
            // Check if any entry was deleted .
        }

        //FOR PICS
        if(resultCode == RESULT_OK && requestCode == REQUEST_IMAGE_CAPTURE && data != null){
            //Save previously generated unique file path in current Placebook entry
            //picLocation = data.getStringExtra(MediaStore.EXTRA_OUTPUT);
            picLocation = photoFile.getAbsolutePath().toString();
            CharSequence showPath = picLocation;
            Toast.makeText(getApplicationContext(), showPath, Toast.LENGTH_SHORT).show();
        }

        //FOR SPEECH
        if(resultCode == RESULT_OK && requestCode == REQUEST_SPEECH_INPUT && data != null) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS );
            // Append result.get(0) to the place name or description text view
            // according to which one had focus when voice recognizer was launched .
            EditText editText = (EditText) findViewById(R.id.edit_place_desc);
            for(int i = 0; i < result.size(); ++i){
                editText.setText(result.get(i) + " ", TextView.BufferType.EDITABLE);
            }
        }

        //FOR LOCATION
        if (resultCode == RESULT_OK && requestCode == REQUEST_PLACE_PICKER && data != null) {
            Place place = PlacePicker.getPlace(data, this);
            //Set place name text view to place.getName().
            EditText editText = (EditText) findViewById(R.id.txtPlaceContent);
            CharSequence placeName = place.getName();
            longitude = place.getLatLng().longitude;
            latitude = place.getLatLng().latitude;
            altitude = 0;

            TextView longTV = (TextView) findViewById(R.id.txtGpsLongitudeContent);
            TextView altTV = (TextView) findViewById(R.id.txtGpsAltitudeContent);
            TextView latTV = (TextView) findViewById(R.id.txtGpsLatitudeContent);

            longTV.setText(String.valueOf(longitude));
            altTV.setText(String.valueOf(altitude));
            latTV.setText(String.valueOf(latitude));

            editText.setText(placeName, TextView.BufferType.EDITABLE);
        }
    }

    // Call initGoogleApi() from MainActivity.onCreate()
    private void initGoogleApi(){
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(new GoogleConnectionCallbacks())
                .addOnConnectionFailedListener(new GoogleApiOnConnectionFailedListener())
                .build();
    }

    // Call launchPlacePicker() when the Pick-A-Place button is clicked.
    private void launchPlacePicker () {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        Context context = getApplicationContext();
        try {
            startActivityForResult(builder.build(context), REQUEST_PLACE_PICKER);
        } catch ( GooglePlayServicesRepairableException e) {
            // Handle exception - Display a Toast message
            CharSequence text = "GooglePlayServices needed to be repaired.";
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        } catch ( GooglePlayServicesNotAvailableException e) {
            // Handle exception - Display a Toast message
            CharSequence text = "Google Play Services not available at this time.";
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }
    }

    private class GoogleConnectionCallbacks implements GoogleApiClient.ConnectionCallbacks{
        @Override
        public void onConnected(Bundle bundle) {
            CharSequence text = "Google API connected.";
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onConnectionSuspended(int i) {
            CharSequence text = "Google API connection suspended.";
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }
    }

    private class GoogleApiOnConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener{
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            // implementation
            CharSequence text = "Google API failed to listen.";
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }
    }


    //WHEN CAMERA BUTTON IS PRESSED
    public void cameraButton(View view){
        dispatchTakePictureIntent();
    }

    //WHEN SPEECH-TO-TEXT BUTTON IS PRESSED
    public void microphoneButton(View view){
        dispatchSpeechInputIntent();
    }

    //WHEN PLACE-PICKER BUTTON IS PRESSED
    public void placePickerButton(View view){
        launchPlacePicker();
    }

    //FOR FINDING ALTITUDE
    public void buttonLocation(View view) {
        getLocation();
    }

    //LOCATION
    private void getLocation(){
        try {
            LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {}

                public void onProviderEnabled(String provider) {}

                public void onProviderDisabled(String provider) {}
            };

            if(!isGPSEnabled && !isNetworkEnabled) {
                CharSequence text = "Location not available";
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            } else {
                if (isNetworkEnabled) {
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            altitude = location.getAltitude();
                        }
                    }
                }
                if(isGPSEnabled) {
                    if(location == null) {
                        if(locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if(location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                altitude = location.getAltitude();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        TextView longTV = (TextView) findViewById(R.id.txtGpsLongitudeContent);
        TextView altTV = (TextView) findViewById(R.id.txtGpsAltitudeContent);
        TextView latTV = (TextView) findViewById(R.id.txtGpsLatitudeContent);

        longTV.setText(String.valueOf(longitude));
        altTV.setText(String.valueOf(altitude));
        latTV.setText(String.valueOf(latitude));

        EditText editText = (EditText) findViewById(R.id.txtPlaceContent);
        editText.setText("(" + String.valueOf(longitude) + ", " + String.valueOf(latitude) + ", " + String.valueOf(altitude) + ")", TextView.BufferType.EDITABLE);

    }



    // Call dispatchTakePictureIntent() when the camera button is clicked.
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent (MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there ’s a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null ) {
            try {
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp + "_";
                File storageDir = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES);
                photoFile = File.createTempFile(
                        imageFileName,  /* prefix */
                        ".jpg",         /* suffix */
                        storageDir      /* directory */
                );
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } catch (IOException e){
                CharSequence text = "Image Does Not Work";
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        } else {
            CharSequence text = "No camera is installed in this device.";
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }
    }

    //Call dispatchSpechInputIntent() when the speech-to-text button is clicked.
    void dispatchSpeechInputIntent(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));
        try{
            startActivityForResult(intent, REQUEST_SPEECH_INPUT);
        } catch ( ActivityNotFoundException a ) {
            // Handle Exception
            CharSequence text = "Microphone failed to execute.";
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }
    }

}

