package jonapostol.placebook;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jon Apostol on 5/26/2015.
 */
public class PlacebookEntry implements Parcelable {
    public long id;
    private String name;
    private String description;
    private String photoPath;
    public PlacebookEntry ( Parcel source ) {
        this.id = source.readLong () ;
        this.name = source.readString();
        this.description = source.readString();
        this.photoPath = source.readString();
    }

    public PlacebookEntry(){

    }

    //GETTERS AND SETTERS
    public void setId(long id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setPhotoPath(String photoPath){
        this.photoPath = photoPath;
    }

    public long getId(){
        return this.id;
    }
    public String getName(){
        return name;
    }
    public String getDescription(){
        return description;
    }
    public String getPhotoPath(){
        return photoPath;
    }

    @Override
    public void writeToParcel ( Parcel dest , int flags ) {
        dest.writeLong(this.id);
        dest.writeString(this.name.toString());
        dest.writeString(this.description);
        dest.writeString(this.photoPath);
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Parcelable.Creator <PlacebookEntry> CREATOR = new Parcelable.Creator<PlacebookEntry>() {

        @Override
        public PlacebookEntry createFromParcel ( Parcel source ) {
            return new PlacebookEntry( source );
        }

        @Override
        public PlacebookEntry [] newArray (int size) {
            return new PlacebookEntry[size];
        }
    };
}